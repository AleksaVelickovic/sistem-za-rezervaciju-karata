import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
import firebase_admin 
from firebase_admin import db, credentials

cred = credentials.Certificate("credentials.json")
firebase_admin.initialize_app(cred, {"databaseURL": "https://bioskop-c2793-default-rtdb.europe-west1.firebasedatabase.app/"})

# ref = db.reference("/korisnici")
# print(ref.get())




class izvor:
    def __init__(self):
        # PROMENJIVE:
        
        self.loggedin = False
        self.managerLoggedIn = False
        self.trenutnoUlogovan = ""
        # self.proveriUsera()

        


        #GUI:

        self.root = tk.Tk()
        self.root.geometry("800x600")
        self.root.title("Bioskop")
        self.root.protocol("WM_DELETE_WINDOW", self.zatvaranje)

        self.sidebar = tk.Frame(self.root, bg="black")

        self.loginbtn = tk.Button(self.sidebar, text="Login/Register", width=15, command=lambda: self.promena(self.loginpage))  #command=lambda: self.test(self.root)
        self.loginbtn.pack(pady=25)

        self.filmovibtn = tk.Button(self.sidebar, text="Filmovi", width=15, command=lambda: self.promena(self.filmovipage))
        self.filmovibtn.pack(pady=25)

        self.podesavanjabtn = tk.Button(self.sidebar, text="Podešavanja", width=15, command=lambda: self.promena(self.podesavanjapage))
        self.podesavanjabtn.pack(pady=25)

        

        self.sidebar.pack(side=tk.LEFT)
        self.sidebar.pack_propagate(False)
        self.sidebar.configure(width=180, height=600)

        self.mainframe = tk.Frame(self.root, highlightbackground="blue", highlightthickness=4)
        self.mainframe.pack(side=tk.LEFT)
        self.mainframe.pack_propagate(False)
        self.mainframe.configure(width=800, height=600)


        
        self.root.mainloop()

        





    # FUNKCIJE:

    def zatvaranje(self):
        if messagebox.askyesno(title="Izlazak", message="Da li ste sigurni da želite da zatvorite aplikaciju?"):
            self.root.destroy()

    def brisanjesadrzaja(self):
        for i in self.mainframe.winfo_children():
            i.destroy()

    def promena(self, page):
        self.brisanjesadrzaja()
        page()

    # STRANICE

    def loginpage(self):
        self.loginframe = tk.Frame(self.mainframe)

        self.labela = tk.Label(self.loginframe, text="Stranica za login\n\n1", font=("Bold", 28))
        self.labela.pack(pady=25)

        self.korisnickoime = tk.Text(self.loginframe, height=1, highlightbackground="green", highlightthickness=2)
        self.korisnickoime.bind("<Button-1>", self.ubaciNavodnike(self.korisnickoime))
        self.korisnickoime.pack()

        self.lozinka = tk.Text(self.loginframe, height=1, highlightbackground="orange", highlightthickness=2)
        self.lozinka.bind("<Button-1>", self.ubaciNavodnike(self.lozinka))
        self.lozinka.pack()

        self.sendbtn = tk.Button(self.loginframe, text="POTVRDI", command=lambda: self.potvrda(eval(self.korisnickoime.get(1.0, tk.END)), eval(self.lozinka.get(1.0, tk.END))))
        self.sendbtn.pack()

        self.logoutbtn = tk.Button(self.loginframe, text="IZLOGUJTE SE", command=self.logout)
        self.logoutbtn.pack()

        self.loginframe.pack(fill="both", expand=1)

    def filmovipage(self):
        self.filmoviframe = tk.Frame(self.mainframe)

        if(self.loggedin==False):
            self.labela = tk.Label(self.filmoviframe, text="NEULOGOVANI KORISNIK", font=("Bold", 28))
            self.labela.pack(pady=25)

        else:
            self.frejm = tk.Frame(self.filmoviframe)
            self.frejm.pack(fill="both", expand=1)

            self.kanvas = tk.Canvas(self.frejm)
            self.kanvas.pack(side="left", fill="both", expand=1)

            self.skrol = ttk.Scrollbar(self.frejm, orient="vertical", command=self.kanvas.yview)
            self.skrol.pack(side="right", fill="y")

            self.kanvas.configure(yscrollcommand=self.skrol.set)
            self.kanvas.bind("<Configure>", lambda e: self.kanvas.configure(scrollregion=self.kanvas.bbox("all")))
            
            self.kanvasfrejm = tk.Frame(self.kanvas)

            self.kanvas.create_window((0, 0), window=self.kanvasfrejm, anchor="nw")

            self.filmovi = db.reference("/filmovi").get()
            self.naziviFilmova = []
            self.zanroviFilmova = []
            self.opisiFilmova = []
            self.reziseriFilmova = []
            self.trajanjaFilmova = []
            self.ulogeFilmova = []
            

            for film in self.filmovi:
                self.naziviFilmova.append(db.reference("/filmovi/" + film + "/naziv").get())
                self.zanroviFilmova.append(db.reference("/filmovi/" + film + "/zanr").get())
                self.opisiFilmova.append(db.reference("/filmovi/" + film + "/opis").get())
                self.reziseriFilmova.append(db.reference("/filmovi/" + film + "/reziser").get())
                self.trajanjaFilmova.append(db.reference("/filmovi/" + film + "/trajanje").get())
                self.ulogeFilmova.append(db.reference("/filmovi/" + film + "/glavneUloge").get())
                

            self.brojac = 0
            self.index = 0
            for film in self.filmovi:
                self.okvirzafilm = tk.Frame(self.kanvasfrejm, highlightbackground="pink", highlightthickness=2)
                self.okvirzafilm.configure(height=300, width=350)
                self.okvirzafilm.grid(column=0, row=self.brojac, padx=200)
                

                self.nazivFilma = tk.Label(self.okvirzafilm, text="Naziv: " + self.naziviFilmova[self.index])
                self.nazivFilma.grid(column=0, row=self.brojac)
                self.brojac+=1

                self.zanrFilma = tk.Label(self.okvirzafilm, text="Zanr: " +  self.zanroviFilmova[self.index])
                self.zanrFilma.grid(column=0, row=self.brojac)
                self.brojac+=1

                self.opisFilma = tk.Label(self.okvirzafilm, text="Opis: " + self.opisiFilmova[self.index])
                self.opisFilma.grid(column=0, row=self.brojac)
                self.brojac+=1

                self.reziserFilma = tk.Label(self.okvirzafilm, text="Rezirao: " + self.reziseriFilmova[self.index])
                self.reziserFilma.grid(column=0, row=self.brojac)
                self.brojac+=1

                self.ulogeUFilmu = tk.Label(self.okvirzafilm, text="Glavne uloge: " + self.ulogeFilmova[self.index])
                self.ulogeUFilmu.grid(column=0, row=self.brojac)
                self.brojac+=1

                self.trajanjeFilma = tk.Label(self.okvirzafilm, text="Trajanje: " + str(self.trajanjaFilmova[self.index]))
                self.trajanjeFilma.grid(column=0, row=self.brojac)
                self.brojac+=1

                self.index+=1

            # self.okvirzafilm2 = tk.Canvas(self.kanvasfrejm, height=300, width=350, highlightbackground="pink")
            # self.okvirzafilm2.grid(column=0, row=1, padx=120)

            # self.okvirzafilm3 = tk.Canvas(self.kanvasfrejm, height=300, width=350, highlightbackground="pink")
            # self.okvirzafilm3.grid(column=0, row=2, padx=120)

            # for i in range(10, 101):
            #     self.labela = tk.Label(self.kanvasfrejm, text=str(i), font=("Bold", 28))
            #     self.labela.grid(column=0, row=i, pady=25, padx=25)

            self.nick = tk.Label(self.okvirzafilm, text="Trenutno ste ulogovani kao: " + self.trenutnoUlogovan)
            self.nick.grid(column=0, row=self.brojac+1)



        self.filmoviframe.pack(fill="both", expand=1)

    def podesavanjapage(self):
        self.podesavanjaframe = tk.Frame(self.mainframe)

        if self.managerLoggedIn == False:
            self.labela = tk.Label(self.podesavanjaframe, text="Samo menadzeri\n mogu pristupiti ovoj stranici", font=("Bold", 28))
            self.labela.pack(pady=25)
        else:
            self.novoMenadzerskoKorIme = tk.Text(self.podesavanjaframe, height=1)
            self.novoMenadzerskoKorIme.bind("<Button-1>", self.ubaciNavodnike(self.novoMenadzerskoKorIme))
            self.novoMenadzerskoKorIme.pack()

            self.novaMenadzerskaLozinka = tk.Text(self.podesavanjaframe, height=1)
            self.novaMenadzerskaLozinka.bind("<Button-1>", self.ubaciNavodnike(self.novaMenadzerskaLozinka))
            self.novaMenadzerskaLozinka.pack()

            self.promeniParametreMenadzeraBTN = tk.Button(self.podesavanjaframe, text="Promeni parametre menadzera", 
                                                        command=lambda: self.promeniMenadzera(eval(self.novoMenadzerskoKorIme.get(1.0, tk.END)), 
                                                                                                eval(self.novaMenadzerskaLozinka.get(1.0, tk.END))))
            self.promeniParametreMenadzeraBTN.pack()

            self.useribtn = tk.Button(self.podesavanjaframe, text="Korisnici...", width=15, command=lambda: self.promena(self.useripage))
            self.useribtn.pack(pady=25)

        self.podesavanjaframe.pack(fill="both", expand=1)
    
    def useripage(self):
        self.useriframe = tk.Frame(self.mainframe)
        self.frejm = tk.Frame(self.useriframe)
        self.frejm.pack(fill="both", expand=1)

        self.kanvas = tk.Canvas(self.frejm)
        self.kanvas.pack(side="left", fill="both", expand=1)

        self.skrol = ttk.Scrollbar(self.frejm, orient="vertical", command=self.kanvas.yview)
        self.skrol.pack(side="right", fill="y")

        self.kanvas.configure(yscrollcommand=self.skrol.set)
        self.kanvas.bind("<Configure>", lambda e: self.kanvas.configure(scrollregion=self.kanvas.bbox("all")))
            
        self.kanvasfrejm = tk.Frame(self.kanvas)

        self.kanvas.create_window((0, 0), window=self.kanvasfrejm, anchor="nw")

        self.useriframe.pack(fill="both", expand=1)

        
        useri = db.reference("/korisnici").get()
        self.KorImena = []
        self.lozinke = []
        for user in useri:
            self.KorImena.append(db.reference("/korisnici/" + user + "/korisnickoIme").get())
            self.lozinke.append(db.reference("/korisnici/" + user + "/lozinka").get())
        brojac = 1
        for i in self.KorImena:
            self.korimenalabela = tk.Label(self.kanvasfrejm, text="Korisnicko ime:", font=("Arial", 20))
            self.korimenalabela.grid(column=0, row=0)
            self.labela = tk.Label(self.kanvasfrejm, text=i, font=("Arial", 20))
            self.labela.grid(column=0, row=brojac)
            brojac+=1
        brojac = 1
        for i in self.lozinke:
            self.lozinkelabela = tk.Label(self.kanvasfrejm, text="Lozinka:", font=("Arial", 20))
            self.lozinkelabela.grid(column=1, row=0)
            self.labela = tk.Label(self.kanvasfrejm, text=i, font=("Arial", 20))
            self.labela.grid(column=1, row=brojac)
            brojac+=1
        # self.useriFajl.close()

        # for i in range(0, 101):
        #         self.labela = tk.Label(self.kanvasfrejm, text=str(i), font=("Bold", 28))
        #         self.labela.grid(column=0, row=i, pady=25, padx=25)

        

    #KRAJ STRANICA

    def potvrda(self, korime, loz):
        if korime == self.proveriMenadzerKorIme() and loz == self.proveriMenadzerLozinku():
            self.loggedin = True
            self.managerLoggedIn = True
            self.trenutnoUlogovan = korime
            messagebox.showinfo(title="ULOGOVAN!", message="Uspešno ste se ulogovali kao menadzer!")
        elif self.proveriUsera(korime, loz) == True:
            self.loggedin = True
            self.trenutnoUlogovan = korime
            messagebox.showinfo(title="ULOGOVAN!", message="Uspešno ste se ulogovali!")    
        else:
            messagebox.showerror(title="GREŠKA", message="Pogrešna lozinka ili korisnčko ime!\n\n" + korime + "\n" + loz)

    def logout(self):
        self.loggedin = False
        self.managerLoggedIn = False
        messagebox.showinfo(title="IZLOGOVAN!", message="Uspešno ste se odjavili iz sistema!")

    def proveriMenadzerKorIme(self):
        menadzerKorIme = db.reference("/menadzer/korisnickoIme").get()
        return menadzerKorIme
    
    def proveriMenadzerLozinku(self):
        menadzerLozinka = db.reference("/menadzer/lozinka").get()
        return menadzerLozinka
        
    def promeniMenadzera(self, korime, loz):
        db.reference("/menadzer").update({"korisnickoIme": korime})
        db.reference("/menadzer").update({"lozinka": loz})
        messagebox.showinfo(title="Gotovo!", message="Parametri menadzera su uspesno promenjeni!")

    def proveriUsera(self, korime, loz):
        useri = db.reference("/korisnici").get()
        self.KorImena = []
        self.lozinke = []

        for user in useri:
            self.KorImena.append(db.reference("/korisnici/" + user + "/korisnickoIme").get())
            self.lozinke.append(db.reference("/korisnici/" + user + "/lozinka").get())
        
        try:
            korimeindex = self.KorImena.index(korime)
            lozinkaindex = self.lozinke.index(loz)
        except:
            return False
        
        if korimeindex == lozinkaindex:
            return True
        else:
            return False
        

        
    def ubaciNavodnike(self, textbox):
        textbox.delete(1.0, tk.END)
        textbox.insert(1.0, '""')
        
        
            
        

    
        

izvor()












# def test(self, arg):
#         arg.geometry("1000x1000")